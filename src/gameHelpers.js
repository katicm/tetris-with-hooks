export const checkCollision = (player, board, { x: moveX, y: moveY }) => {
  for (
    let y = 0;
    y < player.tetromino.shape[player.tetromino.shapeNo].length;
    y += 1
  ) {
    for (
      let x = 0;
      x < player.tetromino.shape[player.tetromino.shapeNo][y].length;
      x += 1
    ) {
      if (player.tetromino.shape[player.tetromino.shapeNo][y][x] !== 0) {
        if (
          !board[y + player.pos.y + moveY] ||
          !board[y + player.pos.y + moveY][x + player.pos.x + moveX] ||
          board[y + player.pos.y + moveY][x + player.pos.x + moveX][1] !==
            "clear"
        ) {
          return true;
        }
      }
    }
  }
  return false;
};

export const Tetrominos = {
  0: { shape: [[[0]]], color: "black" },
  I: {
    shape: [
      [
        [0, "I", 0, 0],
        [0, "I", 0, 0],
        [0, "I", 0, 0],
        [0, "I", 0, 0]
      ],
      [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        ["I", "I", "I", "I"],
        [0, 0, 0, 0]
      ],
      [
        [0, "I", 0, 0],
        [0, "I", 0, 0],
        [0, "I", 0, 0],
        [0, "I", 0, 0]
      ],
      [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        ["I", "I", "I", "I"],
        [0, 0, 0, 0]
      ]
    ],
    color: "aqua"
  },
  J: {
    shape: [
      [
        [0, "J", 0],
        [0, "J", 0],
        ["J", "J", 0]
      ],
      [
        [0, 0, 0],
        ["J", 0, 0],
        ["J", "J", "J"]
      ],
      [
        ["J", "J", 0],
        ["J", 0, 0],
        ["J", 0, 0]
      ],
      [
        [0, 0, 0],
        ["J", "J", "J"],
        [0, 0, "J"]
      ]
    ],
    color: "Fuchsia"
  },
  L: {
    shape: [
      [
        [0, "L", 0],
        [0, "L", 0],
        [0, "L", "L"]
      ],
      [
        [0, 0, 0],
        ["L", "L", "L"],
        ["L", 0, 0]
      ],
      [
        ["L", "L", 0],
        [0, "L", 0],
        [0, "L", 0]
      ],
      [
        [0, 0, "L"],
        ["L", "L", "L"]
      ]
    ],
    color: "orange"
  },
  O: {
    shape: [
      [
        [0, "O", "O"],
        [0, "O", "O"],
        [0, 0, 0]
      ],
      [
        [0, "O", "O"],
        [0, "O", "O"],
        [0, 0, 0]
      ],
      [
        [0, "O", "O"],
        [0, "O", "O"],
        [0, 0, 0]
      ],
      [
        [0, "O", "O"],
        [0, "O", "O"],
        [0, 0, 0]
      ]
    ],
    color: "yellow"
  },
  S: {
    shape: [
      [
        [0, "S", "S"],
        ["S", "S", 0],
        [0, 0, 0]
      ],
      [
        [0, "S", 0],
        [0, "S", "S"],
        [0, 0, "S"]
      ],
      [
        [0, "S", "S"],
        ["S", "S", 0],
        [0, 0, 0]
      ],
      [
        [0, "S", 0],
        [0, "S", "S"],
        [0, 0, "S"]
      ]
    ],
    color: "red"
  },
  T: {
    shape: [
      [
        ["T", "T", "T"],
        [0, "T", 0],
        [0, 0, 0]
      ],
      [
        [0, "T", 0],
        ["T", "T", 0],
        [0, "T", 0]
      ],
      [
        [0, "T", 0],
        ["T", "T", "T"],
        [0, 0, 0]
      ],
      [
        [0, "T", 0],
        [0, "T", "T"],
        [0, "T", 0]
      ]
    ],
    color: "purple"
  },
  Z: {
    shape: [
      [
        ["Z", "Z", 0],
        [0, "Z", "Z"],
        [0, 0, 0]
      ],
      [
        [0, 0, "Z"],
        [0, "Z", "Z"],
        [0, "Z", 0]
      ],
      [
        ["Z", "Z", 0],
        [0, "Z", "Z"],
        [0, 0, 0]
      ],
      [
        [0, 0, "Z"],
        [0, "Z", "Z"],
        [0, "Z", 0]
      ]
    ],
    color: "green"
  }
};

export const randomTetromino = () => {
  const tetrominos = "IJLOSTZ";
  const randTetromino =
    tetrominos[Math.floor(Math.random() * tetrominos.length)];
  return Tetrominos[randTetromino].shape;
};

export const generateNext = () => {
  return [randomTetromino(), randomTetromino(), randomTetromino()];
};

const WIDTH = 10;
const HEIGHT = 20;
export const createBoard = () =>
  Array.from(Array(HEIGHT), () => new Array(WIDTH).fill([0, "clear"]));
