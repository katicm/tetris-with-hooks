import { useState, useEffect } from "react";
import { createBoard } from "../gameHelpers";

export const useBoard = (player, resetTetro, updateNext) => {
  const [board, setBoard] = useState(createBoard());
  const [rowsCleaned, setRowsCleaned] = useState(0);

  useEffect(() => {
    const clearRow = newBoard =>
      newBoard.reduce((ack, row) => {
        if (row.findIndex(cell => cell[0] === 0) === -1) {
          ack.unshift(new Array(newBoard[0].length).fill([0, "clear"]));
          setRowsCleaned(prev => prev + 1);
          return ack;
        }
        ack.push(row);
        return ack;
      }, []);

    const updateBoard = prevBoard => {
      const newBoard = prevBoard.map(row =>
        row.map(cell => (cell[1] === "clear" ? [0, "clear"] : cell))
      );
      player.tetromino.shape[player.tetromino.shapeNo].forEach((row, y) => {
        row.forEach((type, x) => {
          if (type !== 0) {
            newBoard[y + player.pos.y][x + player.pos.x] = [
              type,
              `${player.collided ? "merged" : "clear"}`
            ];
          }
        });
      });
      if (player.collided) {
        resetTetro();
        updateNext();
        return clearRow(newBoard);
      }
      return newBoard;
    };

    setBoard(prev => updateBoard(prev));
  }, [player, resetTetro, updateNext]);

  return [board, setBoard, rowsCleaned, setRowsCleaned];
};
