import { useState, useCallback } from "react";
import { randomTetromino, generateNext } from "../gameHelpers";

export const useTetromino = () => {
  const [nextTetro, setNextTetro] = useState(generateNext());

  const updateNext = useCallback(() => {
    nextTetro.splice(0, 1);
    setNextTetro(nextTetro => [...nextTetro, randomTetromino()]);
  }, [nextTetro]);

  const generateTetro = () => {
    setNextTetro(generateNext());
  };

  return [nextTetro, updateNext, generateTetro];
};
