import { useState, useCallback } from "react";
import { Tetrominos } from "../gameHelpers";

export const usePlayer = nextTetro => {
  const [player, setPlayer] = useState({
    pos: { x: 0, y: 0 },
    tetromino: { shape: Tetrominos[0].shape, shapeNo: 0 },
    collided: false
  });

  const rotateTetro = () => {
    setPlayer(prev => ({
      ...prev,
      tetromino: {
        shape: prev.tetromino.shape,
        shapeNo:
          prev.tetromino.shapeNo === 3 ? 0 : (prev.tetromino.shapeNo += 1)
      }
    }));
  };

  const updateTetroPos = ({ x, y, collided }) => {
    setPlayer(prev => ({
      ...prev,
      pos: { x: (prev.pos.x += x), y: (prev.pos.y += y) },
      collided
    }));
  };

  const resetTetro = useCallback(() => {
    setPlayer({
      pos: { x: 3, y: 0 },
      tetromino: { shape: nextTetro[0], shapeNo: 0 },
      collided: false
    });
  }, [nextTetro]);

  return [player, updateTetroPos, resetTetro, rotateTetro];
};
