import { useState, useEffect } from "react";

export const useResult = (rowsCleaned, numDrop) => {
  const [score, setScore] = useState(0);
  const [level, setLevel] = useState(1);

  useEffect(() => {
    setScore(numDrop * 4 + rowsCleaned * 10);
  }, [numDrop, rowsCleaned]);

  useEffect(() => {
    setLevel(Math.floor(score / 100) + 1);
  }, [score]);

  return [score, level];
};
