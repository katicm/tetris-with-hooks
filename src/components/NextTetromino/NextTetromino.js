import React, { useState, useEffect } from "react";
import Cell from "../Cell/Cell";
import "../NextTetromino/NextTetromino.scss";

const NextTetromino = ({ nextTetro }) => {
  const [nextBoard, setNextBoard] = useState(
    Array.from(Array(15), () => new Array(3).fill(0))
  );
  useEffect(() => {
    const update = prev => {
      var i = 1;
      const newBoard = prev.map(row => row.map(() => 0));
      nextTetro.forEach(el => {
        el[0].forEach(row => {
          for (let x = 0; x < 3; x += 1) {
            newBoard[i][x] = row[x];
          }
          i += 1;
        });
        i += 1;
      });
      return newBoard;
    };
    setNextBoard(prev => update(prev));
  }, [nextTetro]);
  return (
    <div className="next_container">
      {nextBoard.map(r => r.map((c, i) => <Cell key={i} type={c} />))}
    </div>
  );
};
export default NextTetromino;
