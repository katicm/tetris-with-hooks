import React, { useState, useMemo, useEffect, useReducer } from "react";
import Table from "../Table/Table";
import Score from "../Scores/Score";
import ButtonGroup from "../ButtonGroup/ButtonGroup";
import NextTetromino from "../NextTetromino/NextTetromino";
import { createBoard, checkCollision } from "../../gameHelpers";
import { useBoard } from "../../hooks/useBoard";
import { usePlayer } from "../../hooks/usePlayer";
import { useInterval } from "../../hooks/useInterval";
import { useTetromino } from "../../hooks/useTetromino";
import { useResult } from "../../hooks/useResult";
import {
  INITIAL_STATE,
  reducer,
  setIsGameOver,
  setIsTurnIn,
  setIsPaused,
  setStartGame,
  setDropTime,
  setNumDrop
} from "./useGameReducer";
import "./Game.scss";

const Game = () => {
  const [state, dispatch] = useReducer(reducer, INITIAL_STATE);
  const { numDrop, dropTime, isGameOver, isPaused, isTurnIn } = state;
  const [nextTetro, updateNext, generateTetro] = useTetromino();
  const [player, updateTetroPos, resetTetro, rotateTetro] = usePlayer(
    nextTetro
  );
  const [board, setBoard, rowsCleaned, setRowsCleaned] = useBoard(
    player,
    resetTetro,
    updateNext
  );
  const [score, level] = useResult(rowsCleaned, numDrop);
  const [keyPressed, setKeyPressed] = useState(null);

  const startGame = () => {
    setBoard(createBoard());
    generateTetro();
    resetTetro();
    setRowsCleaned(0);
    dispatch(setStartGame());
  };

  const turnGame = () => {
    if (isTurnIn) {
      setBoard(createBoard());
      dispatch(setIsGameOver(true));
    } else {
      startGame();
    }
    dispatch(setIsTurnIn());
  };

  const resetGame = () => {
    if (isTurnIn) {
      startGame();
    }
  };

  const pauseGame = () => {
    dispatch(setIsPaused());
  };
  useEffect(() => {
    isPaused
      ? dispatch(setDropTime(null))
      : dispatch(setDropTime(1000 / level));
  }, [isPaused, level]);

  const verticalMoveTetro = () => {
    if (!checkCollision(player, board, { x: 0, y: 1 })) {
      updateTetroPos({ x: 0, y: 1, collided: false });
    } else {
      if (player.pos.y < 1) {
        dispatch(setIsGameOver(true));
      }
      updateTetroPos({ x: 0, y: 0, collided: true });
      dispatch(setNumDrop());
    }
  };

  const horizontalMoveTetro = dir => {
    if (!checkCollision(player, board, { x: dir, y: 0 })) {
      updateTetroPos({ x: dir, y: 0 });
    }
  };

  const rotateMoveTetro = () => {
    var copyPlayer = JSON.parse(JSON.stringify(player));
    copyPlayer.tetromino.shapeNo === 3
      ? (copyPlayer.tetromino.shapeNo = 0)
      : (copyPlayer.tetromino.shapeNo += 1);
    if (!checkCollision(copyPlayer, board, { x: 0, y: 0 })) {
      rotateTetro();
    }
  };

  const moveTetro = ({ keyCode }) => {
    if (!isGameOver) {
      if (keyCode === 37) {
        horizontalMoveTetro(-1);
      } else if (keyCode === 39) {
        horizontalMoveTetro(1);
      } else if (keyCode === 40) {
        dispatch(setDropTime(null));
        verticalMoveTetro();
      } else if (keyCode === 38) {
        rotateMoveTetro();
      }
    }
    setKeyPressed(keyCode);
  };

  const keyUp = ({ keyCode }) => {
    if (!isGameOver) {
      if (keyCode === 40) {
        dispatch(setDropTime(1000 / level));
      }
    }
  };

  useInterval(
    () => {
      verticalMoveTetro();
    },
    isGameOver ? null : dropTime
  );

  useEffect(() => {
    let interval = setInterval(() => {
      setKeyPressed(null);
    }, 100);
    return () => clearInterval(interval);
  }, [keyPressed]);

  const nextTetrominoMemo = useMemo(() => {
    return (
      <NextTetromino
        nextTetro={isTurnIn ? (isGameOver ? [] : nextTetro) : []}
      />
    );
  }, [nextTetro, isGameOver, isTurnIn]);

  return (
    <div
      onKeyDown={moveTetro}
      onKeyUp={keyUp}
      tabIndex="0"
      className="game-container"
    >
      <div className="game-display">
        <Table board={board}></Table>
        <div>
          {nextTetrominoMemo}
          <Score score={score} level={level} isTurnIn={isTurnIn} />
        </div>
      </div>
      <ButtonGroup
        moveTetro={moveTetro}
        turnGame={turnGame}
        resetGame={resetGame}
        pauseGame={pauseGame}
        buttonUp={keyUp}
        keyPressed={keyPressed}
      />
    </div>
  );
};
export default Game;
