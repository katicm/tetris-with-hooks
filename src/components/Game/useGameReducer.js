export const INITIAL_STATE = {
  dropTime: null,
  isGameOver: true,
  isTurnIn: false,
  isPaused: false,
  numDrop: 0
};

export const reducer = (state, action) => {
  switch (action.type) {
    case "SET_NUMDROP":
      return { ...state, numDrop: state.numDrop + 1 };
    case "START_GAME":
      return {
        ...state,
        dropTime: 1000,
        numDrop: 0,
        isGameOver: false,
        isPaused: false
      };
    case "SET_DROPTIME":
      return { ...state, dropTime: action.payload };
    case "SET_ISGAMEOVER":
      return { ...state, isGameOver: action.payload };
    case "SET_ISTURNIN":
      return { ...state, isTurnIn: !state.isTurnIn };
    case "SET_ISPAUSED":
      return { ...state, isPaused: !state.isPaused };
    default:
      return state;
  }
};

export const setNumDrop = () => ({
  type: "SET_NUMDROP"
});
export const setDropTime = payload => ({
  type: "SET_DROPTIME",
  payload
});
export const setStartGame = () => ({
  type: "START_GAME"
});
export const setIsGameOver = payload => ({
  type: "SET_ISGAMEOVER",
  payload
});
export const setIsTurnIn = () => ({
  type: "SET_ISTURNIN"
});
export const setIsPaused = () => ({
  type: "SET_ISPAUSED"
});
