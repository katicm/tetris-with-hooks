import React from "react";
import { Tetrominos } from "../../gameHelpers";
import "./Cell.scss";

const Cell = ({ type }) => {
  return (
    <div
      style={{ background: Tetrominos[type].color }}
      className="cell-container"
    ></div>
  );
};
export default Cell;
