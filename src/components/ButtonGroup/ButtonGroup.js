import React from "react";
import arrow from "../ButtonGroup/arrow.png";
import "../ButtonGroup/ButtonGroup.scss";

const ButtonGroup = ({
  moveTetro,
  turnGame,
  resetGame,
  pauseGame,
  buttonUp,
  keyPressed
}) => {
  return (
    <div className="buttons-container">
      <div className="buttons-container-left">
        <button
          onClick={() => moveTetro({ keyCode: 37 })}
          className={keyPressed === 37 ? "button-left-active" : "button-left"}
        >
          <img src={arrow} alt="<" />
        </button>
        <label className="label-normal-left">Left</label>
        <button
          onClick={() => moveTetro({ keyCode: 39 })}
          className={keyPressed === 39 ? "button-right-active" : "button-right"}
        >
          <img src={arrow} alt="<" />
        </button>
        <label className="label-normal-right">Right</label>
        <button
          onMouseUp={() => buttonUp({ keyCode: 40 })}
          onMouseDown={() => moveTetro({ keyCode: 40 })}
          className={keyPressed === 40 ? "button-down-active" : "button-down"}
        >
          <img src={arrow} alt="<" />
        </button>
        <label className="label-normal-down">Down</label>
      </div>
      <div className="buttons-container-right">
        <button
          onClick={() => moveTetro({ keyCode: 38 })}
          className={
            keyPressed === 38 ? "button-rotate-active" : "button-rotate"
          }
        >
          <img src={arrow} alt="<" />
        </button>
        <label className="label-normal-rotate">Rotate</label>
        <button className="button-small-start" onClick={turnGame} />
        <button className="button-small-reset" onClick={resetGame} />
        <button className="button-small-pause" onClick={pauseGame} />
        <label className="label-small-start">On/Off</label>
        <label className="label-small-reset">Reset</label>
        <label className="label-small-pause">Pause</label>
      </div>
    </div>
  );
};
export default React.memo(ButtonGroup);
