import React from "react";
import "../Scores/Score.scss";

const Score = ({ score, level, isTurnIn }) => {
  return (
    <div className="score_container">
      {isTurnIn && (
        <>
          <div>Score</div>
          <div>{score}</div>
          <div>Level</div>
          <div>{level}</div>
        </>
      )}
    </div>
  );
};
export default React.memo(Score);
