import React from "react";
import Cell from "../Cell/Cell";
import "./Table.scss";

const Table = ({ board }) => {
  return (
    <div className="table-container">
      {board.map(r => r.map((c, i) => <Cell key={i} type={c[0]} />))}
    </div>
  );
};
export default Table;
