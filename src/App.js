import React from "react";
import "./App.scss";
import Game from "./components/Game/Game";

function App() {
  return (
    <div className="App">
      <div className="App-body">
        <Game />
        <div className="text-info">SORRY, DEVICE SIZE IS NOT SUPPORTED</div>
      </div>
    </div>
  );
}

export default App;
